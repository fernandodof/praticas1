function startTable() {
    $(document).ready(function() {
        $('#apostas').dataTable({
            "language": {
                "lengthMenu": "Exibir _MENU_ registros por pagina",
                "zeroRecords": "Nada encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Sem registros disponíveis",
                "infoFiltered": "(filtrado de _MAX_ total records)",
                "search": "Pesquisar:"
            }

        });
    });
    jQuery(document).ready(function($) {
        $('#tabs').tab();
    });
}