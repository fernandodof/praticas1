/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ipfb.praticas.report;

import br.edu.ifpb.praticas.beans.Pessoa;
import java.util.Collection;
import java.util.Iterator;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Fernando
 */
public class PessoaDataSource implements JRDataSource {

    private Iterator<Pessoa> iterator;
    private Pessoa cursor;

    public PessoaDataSource(Collection<Pessoa> object) {
        super();
        iterator = object.iterator();
    }

    @Override
    public boolean next() throws JRException {
        boolean retorno = iterator.hasNext();
        if(retorno){
            cursor = iterator.next();
        }
        return  retorno;
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        Pessoa pessoa = cursor;
        
        if("id".equals(jrf.getName())){
            return pessoa.getId();
        }else if ("adm".equals(jrf.getName())){
            return pessoa.isAdm();
        }else if ("email".equals(jrf.getName())){
            return pessoa.getEmail();
        }else if ("nome".equals(jrf.getName())){
            return pessoa.getNome();
        }else if ("senha".equals(jrf.getName())){
            return pessoa.getSenha();
        }
        return null;
    }

}
