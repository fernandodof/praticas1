/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.praticas.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author Fernando
 */
@Entity
@Dependent
@SequenceGenerator(name = "seq_pessoa", sequenceName = "Sequencia_De_Pessoa", allocationSize = 1, initialValue = 0)
@NamedQueries({
    @NamedQuery(name="Pessoa.login", query = "SELECT p FROM Pessoa p WHERE p.email = :email AND p.senha = :senha")})
public class Pessoa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_pessoa")
    private int id;
    private String nome;
    @Column(unique = true)
    private String email;
    private String senha;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pessoa_id")
    private List<Aposta> aposta;
    //se verdadeiro o usuário é administrador
    private boolean adm;
      
    public Pessoa() {
    }

    public Pessoa(String nome, String email, String senha, boolean adm) {
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.aposta = new ArrayList();
        this.adm = adm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Aposta> getAposta() {
        return aposta;
    }
    
    public void addAposta(Aposta aposta){
       this.aposta.add(aposta);
    }

    public void setAposta(Aposta aposta) {
        this.aposta = (List<Aposta>) aposta;
    }

    public boolean isAdm() {
        return adm;
    }

    public void setAdm(boolean adm) {
        this.adm = adm;
    }
    
}