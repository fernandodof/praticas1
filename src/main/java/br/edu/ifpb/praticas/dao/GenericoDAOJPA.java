/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.praticas.dao;

import java.io.Serializable;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fernando
 * @param <T>
 */
@Stateless
public class GenericoDAOJPA<T> implements GenericoDAO<T> {

    @PersistenceContext(name = "projetoPraticas")
    private EntityManager em;

    @Override
    public boolean save(T entity) {
        try {
            em.persist(entity);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(T entity) {
        try {
            em.merge(entity);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public T find(Class<T> classType, T entity) {
        return this.em.find(classType, entity);
    }

    @Override
    public boolean delete(T entity) {
        try {
            this.em.remove(entity);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public T getById(Class<T> classTClass, Object id) {
        try {
            return em.find(classTClass, id);
        } catch (NoResultException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public T simpleQuery(String query, Map<Integer, Serializable> map) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T executeNativeQuery(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T getSingleResultOfNamedQuery(String namedQuery, Map<String, Object> map) throws NoResultException {
        Query query = this.em.createNamedQuery(namedQuery);
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String param = entry.getKey();
            Object value = entry.getValue();
            query.setParameter(param, value);
        }
        T t;
        try {
            t = (T) query.getSingleResult();
        } catch (NoResultException ex) {
            t = null;
        }
        return t;
    }

    @Override
    public T getSingleResultOfNamedQuery(String namedQuery) {
        try {
            Query query = this.em.createNamedQuery(namedQuery);
            if (query.getResultList().isEmpty()) {
                throw new NoResultException();
            } else {
                return (T) query.getResultList().get(0);
            }
        } catch (NoResultException ex) {
            return null;
        }
    }
    
}
