package br.edu.ifpb.praticas.controller;

import br.edu.ifpb.praticas.beans.Concurso;
import br.edu.ifpb.praticas.dao.GenericoDAO;
import br.edu.ifpb.praticas.gerador.GeradorDeNumeros;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.SortedSet;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named("ControladorConcurso")
@RequestScoped
public class ControladorConcurso implements Serializable {

    @EJB
    private GenericoDAO genericoDAO;
    private String dataNovoConcurso;
    private String sorteados;
    @Inject
    private Concurso concurso;

    public Concurso getProximoConcurso() {
        return (Concurso) genericoDAO.getSingleResultOfNamedQuery("Concurso.proximos");
    }

    public String novoConcurso() {
        String[] dataVetor = dataNovoConcurso.split("/");
        dataNovoConcurso = dataVetor[2] + "-" + dataVetor[1] + "-" + dataVetor[0] + " 00:00:00";
        Timestamp timestamp = Timestamp.valueOf(dataNovoConcurso);
        concurso.setDataHora(timestamp);
        concurso.setRealizado(false);
        genericoDAO.save(concurso);
        dataNovoConcurso = null;
        return null;
    }

    public String realizarSorteio() {
        Concurso concurso = (Concurso) genericoDAO.getSingleResultOfNamedQuery("Concurso.proximos");
        System.out.println(concurso.isRealizado());
        SortedSet numerosSorteados = GeradorDeNumeros.getSeisNumerosEntreUmESessenta();
        concurso.setNumeros(numerosSorteados);
        concurso.setRealizado(true);
        System.out.println(numerosSorteados.toString());
        genericoDAO.update(concurso);
        this.sorteados = numerosSorteados.toString();
        return null;
    }

    public String getDataNovoConcurso() {
        return dataNovoConcurso;
    }

    public void setDataNovoConcurso(String dataNovoConcurso) {
        this.dataNovoConcurso = dataNovoConcurso;
    }

    public String getSorteados() {
        return sorteados;
    }

    public void setSorteados(String sorteados) {
        this.sorteados = sorteados;
    }

}