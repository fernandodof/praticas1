/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.ifpb.praticas.controller;

import br.edu.ifpb.praticas.beans.Aposta;
import br.edu.ifpb.praticas.beans.Concurso;
import br.edu.ifpb.praticas.beans.Pessoa;
import br.edu.ifpb.praticas.dao.GenericoDAO;
import java.io.Serializable;
import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Fernando
 */

@Named("ControladorAposta")
@SessionScoped
public class ControladorAposta implements Serializable{
    private String numeros;
    private Aposta aposta;
    @EJB
    private GenericoDAO genericoDAO;
    
    public String gerarAposta(){
        String[] arrayNumeros = numeros.split(",");
        SortedSet setNumeros = new TreeSet();
        setNumeros.addAll(Arrays.asList(arrayNumeros));
        aposta.setNumeros(setNumeros);
        aposta.setConcurso((Concurso) genericoDAO.getSingleResultOfNamedQuery("Concurso.proximos"));
        Pessoa pessoa = (Pessoa) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("pessoa");
        System.out.println("Pessoa: "+pessoa.getNome());
        return null;
    }

    public String getNumeros() {
        return numeros;
    }

    public void setNumeros(String numeros) {
        this.numeros = numeros;
    }

    public Aposta getAposta() {
        return aposta;
    }

    public void setAposta(Aposta aposta) {
        this.aposta = aposta;
    }
    
   
    
}