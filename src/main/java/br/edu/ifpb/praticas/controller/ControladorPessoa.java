/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.praticas.controller;

import br.edu.ifpb.praticas.beans.Aposta;
import br.edu.ifpb.praticas.beans.Concurso;
import br.edu.ifpb.praticas.beans.Pessoa;
import br.edu.ifpb.praticas.dao.GenericoDAO;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Fernando
 */
@SessionScoped
@Named("ControladorPessoa")
public class ControladorPessoa implements Serializable {

    @Inject
    private Pessoa pessoa;
    private String email;
    private String senha;
    @Named("Numeros")
    @RequestScoped
    private String numeros;
    @EJB
    private GenericoDAO genericoDAO;

    public String save() {
        pessoa.setAdm(false);
        if (genericoDAO.save(pessoa)) {
            pessoa = new Pessoa();
            return "index";
        }
        return null;
    }

    public String login() {
        Map<String, Object> loginParms = new HashMap();
        loginParms.put("email", email);
        loginParms.put("senha", senha);
        pessoa = (Pessoa) genericoDAO.getSingleResultOfNamedQuery("Pessoa.login", loginParms);
        String result = null;
        if (pessoa != null) {
            if (pessoa.isAdm()) {
                result = "administrador/PaginaPrincipalAdministrador";
            } else {
                result = "apostador/PaginaPrincipalApostador";
            }
        }
        email = null;
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        request.getSession().setAttribute("idPessoa", pessoa.getId());
        request.getSession().setAttribute("adm", pessoa.isAdm());
        return result;
    }

    public String gerarAposta() {
        Aposta aposta = new Aposta();
        String[] arrayNumeros = numeros.split(",");
        SortedSet setNumeros = new TreeSet();
        setNumeros.addAll(Arrays.asList(arrayNumeros));
        aposta.setNumeros(setNumeros);
        aposta.setConcurso((Concurso) genericoDAO.getSingleResultOfNamedQuery("Concurso.proximos"));
        pessoa = (Pessoa) genericoDAO.find(Pessoa.class, pessoa.getId());
        pessoa.addAposta(aposta);
        genericoDAO.update(pessoa);
        numeros = null;
        return null;
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        pessoa = null;
        email = null;
        return "index";
    }
    
    public String editarPessoa(){
        genericoDAO.update(pessoa);
        return null;
    }

    public String getNumeros() {
        return numeros;
    }

    public void setNumeros(String numeros) {
        this.numeros = numeros;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

}
